import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IPost } from './posts/ipost';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private hostUrl = 'http://jsonplaceholder.typicode.com/';

  constructor(private http: HttpClient) { }

  getPosts(): Observable<Array<IPost>> {
    return this.http.get<Array<IPost>>(this.hostUrl + 'posts');
  }

  getPostsByUserId(userId: number): Observable<Array<IPost>> {
    const param = new HttpParams().set('userId', String(userId));
    return this.http.get<Array<IPost>>
      (this.hostUrl + 'posts', { params: param });
  }

  getPostById(id: number): Observable<IPost> {
    return this.http.get<IPost>
      (this.hostUrl + 'posts/' + id);
  }

  addPost(newPost: IPost): Observable<IPost> {
    return this.http.post<IPost>(this.hostUrl + 'posts', newPost);
  }

  updatePost(newPost: IPost): Observable<IPost> {
    return this.http.put<IPost>(this.hostUrl + 'posts/' + newPost.id, newPost);
  }

  deletePost(id: number): Observable<IPost> {
    return this.http.delete<IPost>(this.hostUrl + 'posts/' + id);
  }

  updatePostProperty(post: IPost): Observable<IPost> {
    return this.http.patch(this.hostUrl + 'posts/' + post.id, post);
  }
}
