import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { IPost } from './ipost';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts: IPost[];
  constructor(private httpService: HttpService) { }

  ngOnInit() {
    this.httpService.getPosts().subscribe(
      (postsFromServer: IPost[]) => {
        this.posts = postsFromServer;
      }, (error: HttpErrorResponse) => {
        console.log(error);
        console.error(error.status + ' | ' + error.message);
      });

    this.httpService.getPostsByUserId(5).subscribe(postsFromServer => {
      console.log(postsFromServer);
    });
  }

  getPost(postId: number) {
    this.httpService.getPostById(postId).subscribe(
      postFromServer => {
        console.log(postFromServer);
      });
  }

  addPost() {
    const newPost: IPost = ({
      id: null,
      userId: 1,
      title: 'kurs Angular',
      body: 'jakies body do andular 1 message'
    });

    this.httpService.addPost(newPost).subscribe(newPostFromServer => {
      console.log(newPostFromServer);
    });
  }

  updatePost() {
    const updatePost: IPost = ({
      id: 1,
      userId: 1,
      title: 'kurs Angular',
      body: 'jakies body do andular 1 message'
    });

    this.httpService.updatePost(updatePost).subscribe(newPostFromServer => {
      console.log(newPostFromServer);
    });
  }

  deletePost() {
    this.httpService.deletePost(55).subscribe(deletedPost => {
      console.log(deletedPost);
    });
  }

  patchPost() {
    const p: IPost = ({
      id: 1,
      body: 'tylko body zmień'
    });

    this.httpService.updatePostProperty(p).subscribe(updatedPost => {
      console.log('PATCH');
      console.dir(updatedPost);
    });
  }
}
